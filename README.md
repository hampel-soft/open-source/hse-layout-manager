# hse-layout-manager


## :memo: Description 

A layout system for LabVIEW frontpanel objects (controls) inspired by Qt5 (https://doc.qt.io/qt-5/layout.html).

## :bulb: Documentation

Detailed documentation on how the actual implementation works and how to use the HSE Layout Manager is hosted at [our Dokuwiki](https://dokuwiki.hampel-soft.com/code/open-source/hse-layout-manager).

### :question: FAQ
See our [FAQ](https://dokuwiki.hampel-soft.com/code/open-source/hse-layout-manager/02_faq) for comments on updating versions amongst other things.

## :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.

## :rocket: Installation

Download the latest VI Package at https://www.vipm.io/package/hse_lib_hse_layout_manager/ and install it globally
with the [VI Package Manager](https://www.vipm.io/download/). 

If you want to use the HSE Layout Manager only in a project scope use the Source Distribution.
Then you can copy the `hse-layout-manager` into your project.

The latest release version can also be found at 
https://dokuwiki.hampel-soft.com/code/hse-libraries/hse-layout-manager

### :wrench: Configuration 

No configuration needed.

## :bulb: Usage

tbd.

## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.

## :beers: Credits

* Manuel Sebald
* Joerg Hampel

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details